# Extemp File
The Extemp File Downloader, or Extemp File, was created for one purpose.  A way to quickly and easily download news articles for use in Extemp Debate. Although it was created with Extemp Debate in mind, it can be used as yet another way to consume news \(although that would probably be a very roundabout method of reading the news\). This program was created for the Taylorsville High School Debate team however, anyone is free to use it and to add on to it.

### License
This application uses the Apache 2.0 License.  Any questions with the license can be answered by looking at the license file included in the code.