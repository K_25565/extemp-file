﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using CodeHollow.FeedReader;

namespace ExtempFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        // Global Variables go here
        public ObservableCollection<string> rssList = new ObservableCollection<string>();
        public string savePath = string.Empty;
        public bool autoDownload = false;
        public int refreshTime = 30;
        private static Timer timer = new Timer();
        private bool firstRun = true;
        private string versionNumber = "1.1.0";
        private bool debugMode = false;
        private string dashedLines = "-------------------------------------------------------------------------------------";

        public MainWindow()
        {
            InitializeComponent();
            checkFiles();
            setDefaultValues();
        }

        private void checkFiles()
        {
            // Check to see if the required files exist.  If not, create them.

            if (!File.Exists("rss.txt"))
            {
                File.CreateText("rss.txt");
            }

            if (!File.Exists("config.txt"))
            {
                File.CreateText("config.txt");
            }

            if (!File.Exists("error-log.txt"))
            {
                File.CreateText("error-log.txt");
            }
        }

        private void setDefaultValues()
        {
            // Set variables to default to prevent errors.

            consoleTextbox.Document.Blocks.Clear();
            addFeedTextbox.Text = string.Empty;
            saveLocationTextbox.Text = string.Empty;
            timeLabel.Content = "30 mins";

            readRssList();
            readConfig();

            // Check if the program is in debug mode

            if (debugMode == true)
            {
                debugButton.IsEnabled = true;
                debugButton.Visibility = Visibility.Visible;
                versionNumber += " - DEBUG";
            }

            // Check whether or not to enable the timer

            if (autoDownload == true)
            {
                timer = new Timer((refreshTime * 60) * 1000);
                timer.Start();
                timer.Elapsed += timerElasped;
            }
            else
            {
                timer.Stop();
                timer.Close();
            }

            // Write the default console text

            writeToConsole(dashedLines);
            writeToConsole(""); // This blank line has to be here or else a new line won't start
            writeToConsole("Extemp File Downloader -- Version: " + versionNumber);
            writeToConsole("Made for the Taylorsville High School Debate Team");
            writeToConsole(dashedLines);
        }

        private void readRssList()
        {
            // Read the links provided on rss.txt and put them into the rssListbox.

            int lineCounter = 0;
            string line = string.Empty;

            using (StreamReader rssReader = new StreamReader("rss.txt"))
            {
                while ((line = rssReader.ReadLine()) != null)
                {
                    rssList.Add(line);
                    lineCounter++;
                }
            }

            rssListbox.ItemsSource = rssList;
        }

        private void writeRssList(string textToWrite, bool delete)
        {
            if (delete == true)
            {
                var tempFile = System.IO.Path.GetTempFileName();
                var linesToKeep = File.ReadLines("rss.txt").Where(l => l != textToWrite);

                File.WriteAllLines(tempFile, linesToKeep);

                File.Delete("rss.txt");
                File.Move(tempFile, "rss.txt");
            }
            else
            {
                using (StreamWriter rssWriter = new StreamWriter("rss.txt", true))
                {
                    rssWriter.WriteLine(textToWrite);
                }
            }
        }

        private void readConfig()
        {
            // Try reading the config, auto-set values if config reading fails.

            try
            {
                autoDownload = Boolean.Parse(File.ReadLines("config.txt").First());
            }
            catch
            {
                autoDownload = false;
            }

            try
            {
                refreshTime = Int32.Parse(File.ReadLines("config.txt").Skip(1).First());
            }
            catch
            {
                refreshTime = 30;
            }

            try
            {
                savePath = File.ReadLines("config.txt").Skip(2).First();
            }
            catch
            {
                savePath = string.Empty;
            }

            if (autoDownload == true)
            {
                timeEnableButton.IsChecked = true;
            }

            timeSlider.Value = refreshTime;
            saveLocationTextbox.Text = savePath;
        }

        private void writeConfig()
        {
            var tempFile = System.IO.Path.GetTempFileName();

            using (StreamWriter configWriter = new StreamWriter(tempFile, true))
            {
                configWriter.WriteLine(autoDownload.ToString());
                configWriter.WriteLine(refreshTime.ToString());
                configWriter.WriteLine(savePath);
            }

            File.Delete("config.txt");
            File.Move(tempFile, "config.txt");
        }

        private async void DownloadRssFeeds()
        {
            // Create a list to store the downloads and their statuses.
            // Create a new bool and check when the save path was created.  If an error is thrown, the save path is invalid

            List<Task> taskList = new List<Task>();
            bool validSavePath = true;
            bool firstFeed = true;

            try
            {
                Directory.GetCreationTime(savePath);
            }
            catch
            {
                validSavePath = false;
            }

            // Only continue if we are certain the save path exists

            if (savePath != string.Empty && validSavePath == true)
            {
                // Disable the start button so we don't have fifteen fireings of this function
                // Enable the stop button so we can stop this if needed

                startButton.IsEnabled = false;
                stopButton.IsEnabled = true;

                foreach (string url in rssList)
                {
                    // Try to find the most specific rss feed from the provided url.
                    // Create an rss reader and read the processed url.

                    string feedUrl = processFeedUrl(url);
                    var rssFeed = FeedReader.Read(feedUrl);

                    // If this is the first feed, don't write the first set of dashed lines for consistency purposes

                    if (firstFeed == true)
                    {
                        writeToConsole("Downloading from feed:  " + rssFeed.Title);
                        writeToConsole(dashedLines);

                        firstFeed = false;
                    }
                    else
                    {
                        writeToConsole(dashedLines);
                        writeToConsole("Downloading from feed:  " + rssFeed.Title);
                        writeToConsole(dashedLines);
                    }

                    // Create an easy to use string for the save location

                    string fileLocation = savePath + "\\" + legalizeName(rssFeed.Title, ';') + "\\";

                    // Check if the save location exists.  If not, create it.

                    if (!Directory.Exists(fileLocation))
                    {
                        Directory.CreateDirectory(fileLocation);
                    }

                    foreach (var article in rssFeed.Items)
                    {
                        writeToConsole("Downloading:  " + article.Title);

                        // Create an easy to use string that is the literal location of the file we're going to download.

                        string downloadLocation = fileLocation + legalizeName(article.Title, ';') + ".html";

                        // Check if the file already exists, if it doesn't proceed with the download.  If it does, skip it.

                        if (File.Exists(downloadLocation))
                        {
                            writeToConsole("Skipping...");
                        }
                        else
                        {
                            using (WebClient downloader = new WebClient())
                            {
                                // Add the task to the task list so it can watch its status

                                taskList.Add(Task.Run(() => downloader.DownloadFileAsync(new Uri(article.Link), downloadLocation)));

                                
                            }
                        }
                    }
                }

                // Create a new task to check when all of the tasks in the task list are complete

                Task t = Task.WhenAll(taskList);

                try
                {
                    // Wait until all of the tasks are complete

                    t.Wait(); 
                }
                catch (Exception err)
                {
                    writeErrorToConsole(err.ToString());
                }

                // If all the tasks complete, reset the downloader and send a message to the user.

                if (t.Status == TaskStatus.RanToCompletion)
                {
                    writeToConsole(dashedLines);
                    writeToConsole("Done!");
                    writeToConsole(dashedLines);

                    startButton.IsEnabled = true;
                    stopButton.IsEnabled = false;
                }
                else
                {
                    writeErrorToConsole("Downloads failed to complete!");
                }
            }
            else
            {
                writeErrorToConsole("Invalid save path!");
            }
        }

        private string processFeedUrl(string url)
        {
            var urls = FeedReader.GetFeedUrlsFromUrl(url);

            string feedUrl = string.Empty;

            if (urls.Count() < 1)
            {
                // There aren't any children urls.  This Url is most likely the right url.

                feedUrl = url;
            }
            else if (urls.Count() == 1)
            {
                // There is only one child url that is an rss feed.

                feedUrl = url;
            }
            else if (urls.Count() == 2)
            {
                // The two children urls are usually an rss feed and a comments feed.  We only care about the rss feed (the first one).

                feedUrl = urls.First().Url;
            }
            else
            {
                // The Url isn't specific enough.  Write an error in the console for now
                // In a later version of this application, make it downlaod from all of the urls.
            }

            return feedUrl;
        }

        private string legalizeName(string name, char replacerChar)
        {
            name = name.Replace('\\', replacerChar);
            name = name.Replace('/', replacerChar);
            name = name.Replace(':', replacerChar);
            name = name.Replace('*', replacerChar);
            name = name.Replace('?', replacerChar);
            name = name.Replace('"', replacerChar);
            name = name.Replace('<', replacerChar);
            name = name.Replace('>', replacerChar);
            name = name.Replace('|', replacerChar);

            return name;
        }

        private void writeToConsole(string text)
        {
            consoleTextbox.AppendText(text + "\n");
            consoleTextbox.ScrollToEnd();
        }

        private void writeErrorToConsole(string errorText)
        {
            try
            {
                using (StreamWriter logWriter = new StreamWriter("error-log.txt", true))
                {
                    logWriter.WriteLineAsync(dashedLines + "\n");
                    logWriter.WriteLineAsync("An error has occured whilst trying to download from your urls." + "\n");
                    logWriter.WriteLineAsync("Error Code:  " + "\n");
                    logWriter.WriteLineAsync(errorText + "\n");
                    logWriter.WriteLineAsync(dashedLines + "\n");
                }
            }
            catch
            {
                // Do nothing
            }

            consoleTextbox.AppendText(dashedLines + "\n");
            consoleTextbox.AppendText("An error has occured whilst trying to download from your urls." + "\n");
            consoleTextbox.AppendText("Error Code:  " + "\n");
            consoleTextbox.AppendText(errorText + "\n");
            consoleTextbox.AppendText(dashedLines + "\n");
            consoleTextbox.ScrollToEnd();
        }

        private void addFeedButton_Click(object sender, RoutedEventArgs e)
        {
            if (addFeedTextbox.Text != string.Empty)
            {
                writeRssList(addFeedTextbox.Text, false);
                rssList.Add(addFeedTextbox.Text);

                addFeedTextbox.Text = string.Empty;
            }
        }

        private void removeFeedButton_Click(object sender, RoutedEventArgs e)
        {
            writeRssList(rssListbox.SelectedItem.ToString(), true);
            rssList.Remove(rssListbox.SelectedItem.ToString());
        }

        private void changeButton_Click(object sender, RoutedEventArgs e)
        {
            savePath = saveLocationTextbox.Text;

            writeConfig();
            writeToConsole("The save location has been changed.");
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            // Attempt to download the Rss feeds.  If any error is found, throw and error message in the console

            try
            {
                DownloadRssFeeds();
            }
            catch (Exception err)
            {
                startButton.IsEnabled = true;

                writeErrorToConsole(err.ToString());
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            writeToConsole("I've yet to be implemented.  Don't forget me!");
        }

        private void debugButton_Click(object sender, RoutedEventArgs e)
        {
            writeToConsole("The debug button has been clicked, but there's nothing to report!");
        }

        private void timeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Check if the program just launched so that the program doesn't overwrite the config file before fully reading it

            if (firstRun == true)
            {
                firstRun = false;
            }
            else
            {
                refreshTime = Convert.ToInt32(timeSlider.Value);

                if (Convert.ToInt32(timeSlider.Value) == 1440)
                {
                    timeLabel.Content = "1 day";
                }
                else
                {
                    int hours = (Convert.ToInt32(timeSlider.Value) / 60);
                    int minutes = (Convert.ToInt32(timeSlider.Value) - (hours * 60));

                    if (hours < 1)
                    {
                        timeLabel.Content = minutes + " mins";
                    }
                    else if (minutes == 0)
                    {
                        timeLabel.Content = hours + " hrs";
                    }
                    else
                    {
                        timeLabel.Content = hours + " hrs, " + minutes + " mins";
                    }
                }

                writeConfig();

                if (autoDownload == true)
                {
                    timer.Stop();
                    timer.Close();

                    timer = new Timer((refreshTime * 60) * 1000);
                    timer.Start();
                    timer.Elapsed += timerElasped;
                }
            }
        }

        private void timeEnableButton_Checked(object sender, RoutedEventArgs e)
        {
            if (!autoDownload)
            {
                timer = new Timer((refreshTime * 60) * 1000);
                timer.Start();
                timer.Elapsed += timerElasped;
            }

            autoDownload = true;
            writeConfig();
        }

        private void timeEnableButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (autoDownload)
            {
                timer.Stop();
                timer.Close();
            }            

            autoDownload = false;
            writeConfig();
        }

        private void timerElasped(object source, ElapsedEventArgs e)
        {
            // Attempt to download the Rss feeds.  If any error is found, throw and error message in the console

            try
            {
                DownloadRssFeeds();
            }
            catch (Exception err)
            {
                startButton.IsEnabled = true;

                writeErrorToConsole(err.ToString());
            }
        }
    }
}